﻿app.service('ajaxService', function ($rootScope, $http, $document, $compile) {

    this.scope = null;
    this.init = function (obj) {
        var acc = this;
        var request = $http({
            method: "post",
            url: "php/ajax.php",
            data: obj
        });
        
        request.then(function (response) {
            //console.log(response.data);

            if (obj.type == 'onload') {
                var array = response.data;
                if (array.length == 0) {
                    $rootScope.id = 0;
                    $rootScope.accNotes = 0;
                } else {
                    var idTmp = [];

                    for (var i = 0; i < array.length; i++) {
                        idTmp.push(array[i][2]);
                        var tmp = idTmp[i].split('@');
                        idTmp[i] = parseInt(tmp[1]);
                    }
                    idTmp.sort(function (a, b) {
                        return b - a;
                    });

                    $rootScope.id = idTmp[0] + 1;
                    var box = angular.element(document.querySelector('#mainBox'));

                    $rootScope.accNotes = array.length;

                    for (var i = 0; i < array.length; i++) {
                        $rootScope.contentMsg = array[i].content;
                        var note = $compile("<div id='" + array[i].noteID + "' class='singleNote' singlenote></div>")(acc.scope);
                        note.css({
                            color: 'red',
                            width: array[i].width + 'px',
                            height: array[i].height + 'px',
                            zIndex: array[i].z_index,
                            left: array[i].x + 'px',
                            top: array[i].y + 'px'
                        });

                        box.append(note);

                        var noteObj = {
                            tabName: array[i].tabName,
                            id: array[i].noteID,
                            startX: parseInt(array[i].startX),
                            startY: parseInt(array[i].startY),
                            x: parseInt(array[i].x),
                            y: parseInt(array[i].y),
                            width: parseInt(array[i].width),
                            height: parseInt(array[i].height),
                            text: array[i].content,
                            z_index: parseInt(array[i].z_index)
                        };

                        $rootScope.tabRoot.push(noteObj);
                    }
                }


                $rootScope.contentMsg = 'Msg';
                $rootScope.add = true;

            } else if (obj.type == 'onloadAll') {

                var array = response.data;
                if (array.length == 0) {
                    $rootScope.totalNotes = 0;
                } else {
                    $rootScope.totalNotes = parseInt(array[0].size);
                }

            }

        }, function (response) {

        });

    }

});