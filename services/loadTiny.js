﻿app.service('loadTinyService', function ($document, $rootScope, ajaxService) {

    this.init = function () {
        
        var mainBox = document.getElementById('mainBox');
        var tiny = mainBox.children[0];
        var bottomBar = tiny.children[0].children[3].children[0];


        var btSave = document.createElement('button');
        btSave.innerHTML = 'Save';
        btSave.setAttribute('class', 'tinyBt');
        btSave.addEventListener('click', function () {

            if ($rootScope.textId != null) {

                var doc = document.getElementById($rootScope.textId);
                doc.childNodes[3].innerHTML = tinyMCE.get('textBox').getContent();

                tiny.style.display = 'none';

                var tmp;
                for (var i = 0; i < $rootScope.tabRoot.length; i++) {
                    if ($rootScope.tabRoot[i].id == $rootScope.textId) {
                        tmp = i;
                    }
                }
                $rootScope.tabRoot[tmp].text = tinyMCE.get('textBox').getContent();

                var textObj = {
                    type: 'updateContent',
                    tabName: $rootScope.tabName,
                    noteID: $rootScope.textId,
                    content: tinyMCE.get('textBox').getContent()
                }
                ajaxService.init(textObj);

                $rootScope.textId = null;
                //console.log($rootScope.tabRoot[tmp]);

            }
        });
        bottomBar.appendChild(btSave);


        var btExit = document.createElement('button');
        btExit.innerHTML = 'Exit';
        btExit.setAttribute('class', 'tinyBt');
        btExit.addEventListener('click', function () {

            tiny.style.display = 'none';

        });
        bottomBar.appendChild(btExit);
        
    }

});