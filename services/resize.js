app.service('resizeService', function ($document, $rootScope, dropService) {

    this.init = function (el, resizeX, resizeY) {
        var tabPos;

        for (var i = 0; i < $rootScope.tabRoot.length; i++) {
            if ($rootScope.tabRoot[i].id == el.id) {
                tabPos = i;
            }
        }

        var resizeWidth = $rootScope.tabRoot[tabPos].width;
        var resizeHeight = $rootScope.tabRoot[tabPos].height;

        $document.on('mousemove', function (event) {

            var note = document.getElementById(el.id);
            var rect = note.getBoundingClientRect();
            
            note.style.width = (resizeWidth + event.pageX - resizeX) + 'px';
            note.style.height = (resizeHeight + event.pageY - resizeY) + 'px';

            $rootScope.tabRoot[tabPos].height = rect.height;
            $rootScope.tabRoot[tabPos].width = rect.width;
        });

        $document.on('mouseup', function () {
            el.style.backgroundColor = 'chartreuse';
            dropService.init(el, tabPos);
            $rootScope.move = true;
        });

    }

});