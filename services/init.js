app.service('init', function ($document, $rootScope, resizeService, dragService, ajaxService) {

    this.mainInit = function (el, textMsg) {

        // ----  MOVE  ----
            dragService.init(el[0]);
        // ----    ----


        
        // ----  RESIZE  ----
            var resize = document.createElement("img");
            resize.setAttribute("class", "resizeLogo");
            resize.setAttribute("src", "img/resize.png");
            resize.addEventListener('mousedown', function (event) {
                $rootScope.move = false;
                event.preventDefault();

                var resizeX = event.pageX;
                var resizeY = event.pageY;

                resizeService.init(el[0], resizeX, resizeY);

            });
            el[0].appendChild(resize);
        // ----    ----


           
        // ----  DELETE  ----
            var del = document.createElement("img");
            del.setAttribute("class", "delLogo");
            del.setAttribute("src", "img/delete.png");
            del.addEventListener("click", function () {

                for (var i = 0; i < $rootScope.tabRoot.length; i++) {
                    if ($rootScope.tabRoot[i].id == this.parentNode.id) {
                        $rootScope.tabRoot.splice(i, 1);
                    }
                }

                var parent = document.getElementById("mainBox");
                var rem = document.getElementById(this.parentNode.id);
                parent.removeChild(rem);
                
                var delObj = {
                    type: 'delete',
                    tabName: $rootScope.tabName,
                    noteID: this.parentNode.id
                }
                ajaxService.init(delObj);

                //$rootScope.totalNotes --;
                $rootScope.accNotes --;
            });
            el[0].appendChild(del);
        // ----    ----


         
        // ----  setTEXT  ----
            var setText = document.createElement("img");
            setText.setAttribute("class", "updateLogo");
            setText.setAttribute("src", "img/update.png");
            setText.addEventListener("click", function () {
                
                $rootScope.textId = this.parentNode.id;
                var tiny = document.getElementById('mainBox').children[0];
                tiny.setAttribute('style', 'display:block !important');
                tiny.style.zIndex = $rootScope.maxIndex;

                var upBox = document.getElementById(this.parentNode.id);
                window.parent.tinymce.get('textBox').setContent(upBox.childNodes[3].innerHTML);
                
            });
            el[0].appendChild(setText);
        // ----    ----


        
        // ----  CONTENT  ----
            var content = document.createElement("div");
            content.setAttribute("class", "noteContent");
            content.innerHTML = textMsg;
            el[0].appendChild(content);
        // ----    ----
            
    }

});