app.service('dropService', function ($document, $rootScope, ajaxService) {

    this.init = function (el, tabPos) {
        
        $document.off('mousemove');
        $document.off('mouseup');

        var textObj = {
            type: 'updatePos',
            data: $rootScope.tabRoot[tabPos]
        }
        ajaxService.init(textObj);
        
    }

});