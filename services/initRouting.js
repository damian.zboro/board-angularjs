﻿app.service('iniRoutingService', function ($document, $rootScope, ajaxService) {
    this.init = function () {
        var tabName = document.getElementById('tabName');
        var name = tabName.value;

        if (name != '') {

            var mainHref = document.getElementById('mainHref');
            mainHref.setAttribute('href', '#!table/' + name);

            var obj = {
                'type': 'onload',
                'tabName': name
            };

            setTimeout(function(){
                ajaxService.init(obj);
            }, 100);

            var objAll = {
                'type': 'onloadAll',
                'tabName': name
            };

            ajaxService.init(objAll);
        }
    }

});