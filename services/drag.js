app.service('dragService', function ($document, $rootScope, dropService) {

    this.init = function (el) {

        el.addEventListener('mousedown', function (event) {
            event.preventDefault();
            this.style.backgroundColor = '#90caf9';
            var thisEl = this;
            var tabPos;
            var tabIndex = [];
            
            for (var i = 0; i < $rootScope.tabRoot.length; i++) {
                if ($rootScope.tabRoot[i].id == this.id) {
                    tabPos = i;
                }
            }
            
            for (var i = 0; i < $rootScope.tabRoot.length; i++) {
                if ($rootScope.tabRoot[i].z_index > $rootScope.tabRoot[tabPos].z_index) {
                    tabIndex.push(i);
                }
            }
            
            var sortTab = [];      
            for(var z=0; z<$rootScope.tabRoot.length; z++){
                sortTab.push($rootScope.tabRoot[z].z_index);
            }
            
            var sort = sortTab.sort(function (a, b) {
                return b-a;
            });

            var tmpZ = sort[0];
            $rootScope.maxIndex = tmpZ + 1;
            
            for(var i=0; i<tabIndex.length; i++){
                
                var tmp = tabIndex[i];
                var tmpIndex = 0;
                if($rootScope.tabRoot[tmp].z_index > 0 ) {
                    tmpIndex = $rootScope.tabRoot[tmp].z_index - 1;
                }
                document.getElementById($rootScope.tabRoot[tmp].id).style.zIndex = tmpIndex;
                $rootScope.tabRoot[tmp].z_index = tmpIndex;

            }

            $rootScope.tabRoot[tabPos].z_index = tmpZ;
            this.style.zIndex = tmpZ;
            
            
            if ($rootScope.move) {

                $rootScope.tabRoot[tabPos].startX = event.pageX - $rootScope.tabRoot[tabPos].x;
                $rootScope.tabRoot[tabPos].startY = event.pageY - $rootScope.tabRoot[tabPos].y;

                $document.on('mousemove', function (event) {

                    $rootScope.tabRoot[tabPos].x = event.pageX - $rootScope.tabRoot[tabPos].startX;
                    $rootScope.tabRoot[tabPos].y = event.pageY - $rootScope.tabRoot[tabPos].startY;

                    thisEl.style.left = $rootScope.tabRoot[tabPos].x + 'px';
                    thisEl.style.top = $rootScope.tabRoot[tabPos].y + 'px';
                    
                });
                $document.on('mouseup', function () {
                    dropService.init(thisEl, tabPos);
                    thisEl.style.backgroundColor = 'chartreuse';
                });

            }
        });

    }

});