﻿app.controller('NoteController', function ($rootScope, $scope, $routeParams, $location, loadTinyService) {

    //  ---- tinymce ---- //

    tinymce.init({
        selector: 'textarea#textBox',
        theme: 'modern',
        width: window.innerWidth * 0.6,
    });

    setTimeout(function(){
        loadTinyService.init();
    }, 50);

    // ----    ----

    $scope.name = 'NoteController';
    $scope.params = $routeParams;
    $rootScope.tabName = $location.path().split("/")[2];

});