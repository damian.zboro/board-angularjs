﻿var app = angular.module("myApp", ["ngRoute"]);


app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
    .when("/", {
        templateUrl: "template/start.html"
    })
    .when('/table/:tableId', {
        templateUrl: 'template/main.html',
        controller: 'NoteController',
    })
    .otherwise({ redirectTo: '/' });
});

app.controller('MainController', function ($route, $routeParams, $location, $scope, $document, $compile, $rootScope, iniRoutingService, ajaxService) {
    $scope.$route = $route;
    $scope.$routeParams = $routeParams;
    $scope.params = $routeParams;

    ajaxService.scope = $scope;
    $rootScope.add = false;

    $rootScope.tabName = null;
    $rootScope.id = 0;

    $rootScope.totalNotes;
    $rootScope.accNotes;

    $rootScope.maxIndex;

    $scope.addNote = function () {
        if ($rootScope.tabName != null) {

            var mainBox = angular.element(document.querySelector('#mainBox'));
            var newNote = $compile("<div id='note@" + $rootScope.id + "' class='singleNote' singlenote></div>")($scope);
            newNote.css({
                color: 'red',
                zIndex: $rootScope.id
            });
            mainBox.append(newNote);
            $rootScope.id++;
            
            $rootScope.totalNotes ++;
            $rootScope.accNotes ++;
        }
    }

    $rootScope.tabRoot = [];
    $rootScope.move = true;
    $rootScope.textId = null;


    $rootScope.send = function () {
        iniRoutingService.init();
    }

    $rootScope.contentMsg = 'Msg';

});

app.directive('singlenote', function ($document, $rootScope, init, ajaxService) {
    return {
        link: function (scope, element, attr) {

            if ($rootScope.tabName != null) {

                if ($rootScope.add) {
                    var obj = {
                        tabName: $rootScope.tabName,
                        id: element[0].id,
                        startX: 0,
                        startY: 0,
                        x: 100,
                        y: 100,
                        width: 200,
                        height: 200,
                        text: $rootScope.contentMsg,
                        z_index: $rootScope.id
                    };

                    $rootScope.tabRoot.push(obj);

                    ajaxService.init({
                        'type': 'add',
                        'noteData': obj
                    });

                }

                init.mainInit(element, $rootScope.contentMsg);
                //console.log($rootScope.tabRoot);
            }

        }
    };
});
