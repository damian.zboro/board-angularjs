﻿<?php

  header('Content-Type: application/json');
  include("loginDB.php");

  try {
      $dbh = new PDO($host, $user, $passwd);
      $dbh->exec("set names utf8");
  } catch (PDOException $e) {
      echo 'Connection failed: ' . $e->getMessage();
  }

  $data = file_get_contents("php://input");
  $req = json_decode($data);
  $type = $req->type;

  switch ($type) {
    case 'onload':
        $name = $req->tabName; 

        $sth = $dbh->prepare("SELECT * FROM notes WHERE tabName=:tabName");
        $sth->bindValue(':tabName', $req->tabName, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetchAll();
        
        echo json_encode($result); 
      break;

    case 'add':
        $obj = $req->noteData;
        $sth = $dbh->prepare('INSERT INTO notes(tabName, noteID, height, width, startX, startY, x, y, content, z_index) VALUES(:tabName, :noteID, :height, :width, :startX, :startY, :x, :y, :content, :z_index);');

        $sth->bindValue(':tabName', $obj->tabName, PDO::PARAM_STR);
        $sth->bindValue(':noteID', $obj->id, PDO::PARAM_STR);
        $sth->bindValue(':height', $obj->height, PDO::PARAM_STR);
        $sth->bindValue(':width', $obj->width, PDO::PARAM_STR);
        $sth->bindValue(':startX', $obj->startX, PDO::PARAM_STR);
        $sth->bindValue(':startY', $obj->startY, PDO::PARAM_STR);
        $sth->bindValue(':x', $obj->x, PDO::PARAM_STR);
        $sth->bindValue(':y', $obj->y, PDO::PARAM_STR);
        $sth->bindValue(':content', $obj->text, PDO::PARAM_STR);
        $sth->bindValue(':z_index', $obj->z_index, PDO::PARAM_STR);
        $sth->execute();

        $z = $dbh->prepare("SELECT * FROM allnotes WHERE tabName=:tabName");
        $z->bindValue(':tabName', $obj->tabName, PDO::PARAM_STR);
        $z->execute();
        $result = $z->fetchAll();
        
        $tabSize = count($result);
        if($tabSize == 0){
          $sthh = $dbh->prepare('INSERT INTO allnotes(tabName, size) VALUES(:tabName, 1);');
          $sthh->bindValue(':tabName', $obj->tabName, PDO::PARAM_STR);
          $sthh->execute();
        }elseif($tabSize > 0){
          $upSize = $dbh->prepare("UPDATE allnotes SET size = size + 1 WHERE tabName='".$obj->tabName."'");
          $upSize->execute();
        }

      break;

    case 'delete':
        $sth = $dbh->prepare('DELETE FROM notes WHERE tabName=:tabName AND noteID=:noteID');
        $sth->bindValue(':tabName', $req->tabName, PDO::PARAM_STR);
        $sth->bindValue(':noteID', $req->noteID, PDO::PARAM_STR);
        
        $sth->execute();

      break;

    case 'updatePos':
        $obj = $req->data;
        $maxIndex = $obj->z_index;

        $sth = $dbh->prepare("SELECT * FROM notes WHERE tabName=:tabName AND noteID=:noteID");
        $sth->bindValue(':tabName', $obj->tabName, PDO::PARAM_STR);
        $sth->bindValue(':noteID', $obj->id, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetchAll();

        $tmpIndex = $result[0]['z_index'];
        

        $reqtab = $dbh->prepare("SELECT * FROM notes WHERE tabName=:tabName AND z_index>:z_index");
        $reqtab->bindValue(':tabName', $obj->tabName, PDO::PARAM_STR);
        $reqtab->bindValue(':z_index', $tmpIndex, PDO::PARAM_STR);
        $reqtab->execute();

        $resTab = $reqtab->fetchAll();
        
        for ($i=0; $i < count($resTab); $i++) { 

          if($resTab[$i]['z_index'] > 0){
            $update = $dbh->prepare("UPDATE notes SET z_index=z_index-1 WHERE ID=:ID");
            $update->bindValue(':ID', $resTab[$i]['ID'], PDO::PARAM_STR);
            $update->execute();
          }

        }


        $pos = $dbh->prepare('UPDATE notes SET x=:x, y=:y, startX=:startX, startY=:startY, z_index=:z_index, height=:height, width=:width WHERE tabName=:tabName AND noteID=:noteID');
        $pos->bindValue(':tabName', $obj->tabName, PDO::PARAM_STR);
        $pos->bindValue(':noteID', $obj->id, PDO::PARAM_STR);
        $pos->bindValue(':startX', $obj->startX, PDO::PARAM_STR);
        $pos->bindValue(':startY', $obj->startY, PDO::PARAM_STR);
        $pos->bindValue(':x', $obj->x, PDO::PARAM_STR);
        $pos->bindValue(':y', $obj->y, PDO::PARAM_STR);
        $pos->bindValue(':z_index', $maxIndex, PDO::PARAM_STR);
        $pos->bindValue(':height', $obj->height, PDO::PARAM_STR);
        $pos->bindValue(':width', $obj->width, PDO::PARAM_STR);
        
        $pos->execute();


      break;  

      
    case 'updateContent':
        $sth = $dbh->prepare('UPDATE notes SET content=:content WHERE tabName=:tabName AND noteID=:noteID');
        $sth->bindValue(':content', $req->content, PDO::PARAM_STR);
        $sth->bindValue(':tabName', $req->tabName, PDO::PARAM_STR);
        $sth->bindValue(':noteID', $req->noteID, PDO::PARAM_STR);
        
        $sth->execute();
      
      break;
     
    case 'onloadAll':
      
        $sth = $dbh->prepare("SELECT * FROM allnotes WHERE tabName=:tabName");
        $sth->bindValue(':tabName', $req->tabName, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetchAll();
              
        echo json_encode($result); 
      break;

  }

?>